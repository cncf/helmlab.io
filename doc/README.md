# Using GitLab's helm.gitlab.io

Documentation Organization:
- [Weekly demos preparation](preparation/README.md)
- [Installation](installation/README.md)
- [Advanced Configuration](advanced/README.md)
- [Charts](charts/README.md) (laid out as the charts are)
- [Minikube](minikube/README.md)
- [Helm](helm/README.md)
- [Let's Encrypt via kube-lego](kube-lego/README.md)
