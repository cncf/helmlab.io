{{- if .Values.enabled }}
apiVersion: batch/v1
kind: Job
metadata:
  name: {{ template "migrations.jobname" . }}
  labels:
    app: {{ template "migrations.name" . }}
    chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
  annotations:
    "helm.sh/hook": post-install,post-upgrade
    "helm.sh/hook-delete-policy": hook-succeeded
spec:
  template:
    metadata:
      labels:
        app: {{ template "migrations.name" . }}
        release: {{ .Release.Name }}
    spec:
      securityContext:
        runAsUser: 1000
        fsGroup: 1000
      initContainers:
        - name: configure
          command: ['sh', '/config/configure']
          image: busybox
          volumeMounts:
          - name: migrations-config
            mountPath: /config
            readOnly: true
          - name: init-migrations-secrets
            mountPath: /init-config
            readOnly: true
          - name: migrations-secrets
            mountPath: /init-secrets
            readOnly: false
      restartPolicy: OnFailure
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          args:
            - /scripts/wait-for-deps
            - /scripts/db-migrate
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: CONFIG_TEMPLATE_DIRECTORY
              value: '/var/opt/gitlab/templates'
            - name: CONFIG_DIRECTORY
              value: '/var/opt/gitlab/config/gitlab/'
            {{- if .Values.initialRootPassword}}
            - name: GITLAB_ROOT_PASSWORD
              value: {{ include "migrations.validate_root_password" . | required "migrations.initialRootPassword needs to be more than 6 characters" | quote }}
            {{- end }}
          volumeMounts:
            - name: migrations-config
              mountPath: '/var/opt/gitlab/templates'
            - name: migrations-secrets
              mountPath: '/etc/gitlab'
              readOnly: true
          resources:
{{ toYaml .Values.resources | indent 12 }}
      volumes:
      - name: migrations-config
        configMap:
          name: {{ template "migrations.fullname" . }}
      - name: init-migrations-secrets
        projected:
          defaultMode: 0400
          sources:
          - secret:
              name: {{ .Values.redis.password.secret }}
              items:
              - key: {{ .Values.redis.password.key }}
                path: redis/password
          - secret:
              name: {{ .Values.psql.password.secret }}
              items:
                - key: {{ .Values.psql.password.key }}
                  path: postgres/psql-password
      - name: migrations-secrets
        emptyDir:
          medium: "Memory"
    {{- if .Values.nodeSelector }}
      nodeSelector:
{{ toYaml .Values.nodeSelector | indent 8 }}
    {{- end }}
{{- end }}
